TEAM

Gary Xian
fxian1

Jerry Wen
fwen2

Vivek Gopalakrishnan
vgopala4


DESIGN

For functions in the Chess class, we added three new public methods: one for Chess Deep Copy Constructor, one to set the turn of the chess class, and one to check if a piece can make a move on some position on the board to see if game is in stalemate. A deep copy is made so that we can check if the state of the chess game is in check without changing any piece positions on the original board. This allows us to also bypass the issue where we cannot call make_move() function inside Chess functions that are declared const (meaning that we cannot change the original chess object). We use the deep copy chess object in functions like in_check(), in_mate(), and in_stalemate(). The set_turn() function is mainly used in the read in function of the chessboard, where the player turn is indiciated at the end of the file to be loaded and needs to be set in the new Chess object created. The check_possible_move() function serves to reduce code repitition for the in_stalemate() function. This function takes in the position of piece to check then runs a for loop for each position on the chess board using make_move() to see if any move can be made. This is also done on Deep Copy of Chess to avoid any real changes to the original board itself.

For make_move() function implementation, we first make a pointer to the starting piece. Then we check various scenarios like if the piece exists (aka not nullptr), if the piece aligns with player's turn, and if a legal move can be made from its position to the user input end position. When all these conditions are passed and the new move on copy board does not put the player in check, then the change is commmitted on the real board. The is_white_turn variable is changed to the opposite boolean at the end of the commit. In the in_check() function, we first retreive the position of the player's king piece, and then check every piece of the opposing player and see if it can successfully capture the king piece. If not, return false and signal player is not in check. The in_mate() function first calls the in_check() function. If it is in check, then proceed to see if player can make a move on any exisiting piece to get out of the check condition. The in_stalemate() function checks if the player can a legal move on any piece and only returns true if no legal move can be made. The in-stream operator takes in characters from an instream and inserts the pieces onto the board. We can simply use '-' to make default nullptr signaling for empty spot on the board.


COMPLETENESS

All methods are fully implemented.
