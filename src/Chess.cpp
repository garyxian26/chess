// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Chess.h"
#include <iostream>
#include <set>
#include <exception>
#include <stdexcept>

using std::set;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

Chess::~Chess() {

}
/**
void Chess::delete_board() {
	const Piece* piece;
	pair<char,char> pos;
	for (char r = '8'; r >= '1'; r--) {
  		for (char c = 'A'; c <= 'H'; c++) {
  			pos = make_pair(c,r);
  			piece = board(pos);
  			if (piece != nullptr) {
  				board.delete_piece(pos);
  			}
		}
	}
}
*/
//deep copy constructor
Chess::Chess(const Chess &obj) : is_white_turn(obj.is_white_turn) {

	for (char r = '8'; r >= '1'; r--) {
		for (char c = 'A'; c <= 'H'; c++) {

			// For each cell on the board, get the piece at that location
			std::pair<char, char> position(c, r);
			const Piece *piece = obj.board(position);

			if (piece != nullptr) {
				char designator = piece->to_ascii();
				board.add_piece(position, designator);
			}
		}
	}
}


bool Chess::make_move(pair<char, char> start, pair<char, char> end) {
  // Get a constant reference to the board
  const Piece *startpiece;
  const Piece *endpiece;

  //Cannot make move on same position
  if (start == end) {
  	return false;
  }

  // Get constant pointers to the pieces at the start and end positions
  try {
    startpiece = board(start);
    endpiece = board(end);
  } catch (const std::invalid_argument &ex) {
    // If either `start` or `end` are invalid locations, return false
		cerr << "Inputs are not valid.";
    return false;
  }

  // Does startpiece exist?
  if (startpiece == nullptr) {
    // Cannot make move if starting piece does not exist
		cerr << "Startpiece does not exist.";
    return false;
  }

	// Ensure player is making a valid move...
	if (!startpiece->legal_capture_shape(start, end)) {
		// Move is illegal
		cerr << "Illegal move shape.";
		return false;
	}
	bool obstacle = piece_in_way(start, end);
	if (obstacle) {
		cerr << "There is a piece in the way.";
		return false;
	}
  // Now that we have validated the inputs, we get to the business of moving
  // pieces...
	if (!make_move_helper(turn_white(), startpiece, endpiece, start, end)) {
		cerr << "Move failed.";
		return false;
	}
	return true;
}

bool Chess::make_move_helper(bool white, const Piece* startpiece, const Piece* endpiece, pair<char,char> start, pair<char,char> end) {
    char starttype;
    if (white != startpiece->is_white()) {
      // Player is trying to move other color piece
      return false;
    }

    if (endpiece && white == endpiece->is_white()) {
        // Player is trying to capture a white piece during white's turn
        return false;
    }
	// Correct pawn movements
	starttype = startpiece->to_ascii();
	if (starttype == 'P' || starttype == 'p') {
		int dist_hor = abs(end.first - start.first);
		if (dist_hor == 1 && endpiece == nullptr) {
			return false;
		}
		else if (dist_hor == 0 && endpiece != nullptr) {
			return false;
		}
	}
    // Make the move
	Chess chess_copy(*this);
	chess_copy.board.delete_piece(start);

    if (endpiece != nullptr) {
      chess_copy.board.delete_piece(end);
    }

	chess_copy.board.add_piece(end, starttype);

    // Ensure the move did not put player in check
    if (chess_copy.in_check(turn_white())) {
			return false;
    }

	this->board.delete_piece(start);

	if (endpiece != nullptr) {
    	this->board.delete_piece(end);
    }
		this->board.add_piece(end, starttype);
		pawn_into_queen(end);
    	this->is_white_turn = !white;
		//chess_copy.delete_board();
		return true;
}

void Chess::pawn_into_queen(pair<char, char> end) {
	//Get the piece
	const Piece *piece;
	piece = board(std::pair<char, char>(end.first, end.second));
	if (piece == nullptr) {
		return;
	}

	char pawn_type = piece->to_ascii();
	if (pawn_type == 'P' && end.second == '8') {
		//create piece queen for white;
		this->board.delete_piece(end);
		board.add_piece(std::pair<char, char>(end.first, end.second) , 'Q' );
	} else if (pawn_type == 'p' && end.second == '1') {
		//create piece queen for black;
		this->board.delete_piece(end);
		board.add_piece(std::pair<char, char>(end.first, end.second) , 'q' );
	} else {
		return;
	}
}

bool Chess::in_check(bool white) const {
	pair<char, char> king_pos;
	pair<char, char> attacker_pos;
	const Piece *attacker;
	const Piece *piece;

  // Locate the correct color king...
  for (char r = '8'; r >= '1'; r--) {
  	for (char c = 'A'; c <= 'H'; c++) {

      // For each cell on the board, get the piece at that location
      piece = board(std::pair<char, char>(c, r));
      if (piece) {

        // If the piece exists, get its designator
        char designator = piece->to_ascii();
        // If it's a king, make sure the color is correct and obtain the
        // coordinates
        if (white && (designator == 'K')) {
          king_pos = make_pair(c, r);
        }
        if (!white && (designator == 'k')) {
          king_pos = make_pair(c, r);
        }
      }
    }
  }

  // Iterate through all pieces on the board, looking for threats to king...
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {

			// For each cell on the board, get the piece at that location
      attacker_pos = make_pair(c, r);
      attacker = board(attacker_pos);

      if (attacker != nullptr) {
				// If attacker exists...
        if (white != attacker->is_white() &&
            attacker->legal_capture_shape(attacker_pos, king_pos) &&
						!piece_in_way(attacker_pos,king_pos)) {
					// If attacker and target king are different colors AND
					// the attacker can legally capture the target king, RETURN true
          return true;
        }
      }
    }
  }
  return false;
}


bool Chess::in_mate(bool white) const {
	pair<char, char> start;
	const Piece *startpiece;

	if (!in_check(white)) {
		return false;
	}

	for (char r = '8'; r >= '1'; r--) {
    	for (char c = 'A'; c <= 'H'; c++) {
      		start = make_pair(c, r);
      		startpiece = board(start);
			if (startpiece != nullptr) {
				if (white == startpiece->is_white()) {
					if (check_possible_move(start)) {
						return false;
					  }
				}
			}
		}
	}
	return true;
}


bool Chess::in_stalemate(bool white) const {
	bool make_move = false;
	pair<char, char> piecepos;
	const Piece* temp;

	for (char r = '8'; r >= '1'; r--) {
		for (char c = 'A'; c <= 'H'; c++) {

			piecepos = make_pair(c, r);
			temp = board(piecepos);

			if (temp != nullptr) {
				if (white == temp->is_white()) {
					make_move = check_possible_move(piecepos);
					if (make_move == true) {
						return false;
					}
				}
			}
		}
	}
	// if (temp != nullptr) {
	// 	this->board.delete_piece(piecepos);
	// }
	return true;
}

//Helper Function for checkmate and stalemate
//returns true if a possible move can be made by this piece and signals non-stalemate
bool Chess::check_possible_move(pair<char,char> piecepos) const {
	pair<char, char> piecepos1;

	for (char r = '8'; r >= '1'; r--) {
		for (char c = 'A'; c <= 'H'; c++) {
			piecepos1 = make_pair(c, r);
			if (piecepos1 != piecepos) {
				Chess chess_copy(*this);
				if (chess_copy.make_move(piecepos,piecepos1)) {
					if (!chess_copy.in_check(turn_white())) {
						return true;
					}
				}
			}
			// delete &chess_copy;
		}
	}
	return false;
}

bool Chess::piece_in_way(std::pair<char, char> start, std::pair<char, char> end) const {
	//Obtain the horizontal and vertical movements
	int dist_hor = abs(end.first - start.first);
	int dist_vert = abs(end.second - start.second);

	//Then we want to decide if this piece is moving horizontally, vertically
	//or diagonally.
	if (dist_hor > 0 && dist_vert == 0) {
		return piece_in_way_hor(start, end);
	} else if (dist_vert > 0 && dist_hor == 0) {
		return piece_in_way_vert(start, end);
	} else {
		return piece_in_way_diag(start, end);
	}

}

bool Chess::piece_in_way_hor(std::pair<char, char> start, std::pair<char, char> end) const {

	Board board = get_board();
	const Piece *piece;

	// This is when the ending position is on the right of the starting position.
	if (end.first > start.first) {
		for (char i = start.first + 1; i < end.first; i++) {
			// create piece at that location
			piece = board(std::pair<char, char>(i, start.second));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}

	// This is when the ending position is on the left of the starting position.
	else {
		for (char i = start.first - 1; i < end.first; i--) {
			//create piece at that location
			piece = board(std::pair<char, char>(i, start.second));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}
}

bool Chess::piece_in_way_vert(std::pair<char, char> start, std::pair<char, char> end) const {

	Board board = get_board();
	const Piece *piece;

	//This is when the ending position is above the starting position.
	if (end.second > start.second) {
		for (char j = start.second + 1; j < end.second; j++) {
			//create piece at that location
			piece = board(std::pair<char, char>(start.first, j));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}

	//This is when the ending position is below the starting position.
	else {
		for (char j = start.second - 1; j > end.second; j--) {
			//create piece at that location
			piece = board(std::pair<char, char>(start.first, j));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}
}

bool Chess::piece_in_way_diag(std::pair<char, char> start, std::pair<char, char> end) const {

	// Obtain the distance; horizontal distance and vertical distances should be the same
	int dist = abs(end.first - start.first);
	// int dist_vert = abs(end.second - start.second);

	Board board = get_board();
	const Piece *piece;

	// This is when the ending position is northeast of the starting position.
	if (end.first > start.first && end.second > start.second) {
		for (int i = 1; i < dist; i++) {
			// create piece at that location
			piece = board(std::pair<char, char>(start.first+i, start.second+i));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}

	// This is when the ending position is southeast of the starting position.
	else if (end.first > start.first && end.second < start.second) {

		for (int i = 1; i < dist; i++) {

			// create piece at that location
			piece = board(std::pair<char, char>(start.first+i, start.second-i));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}

	//This is when the ending position is southwest of the starting position.
	else if (end.first < start.first && end.second < start.second) {
		for (int i = 1; i < dist; i++) {
			//create piece at that location
			piece = board(std::pair<char, char>(start.first-i, start.second-i));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}

	//This is when the ending position is northwest of the starting position.
	else if (end.first < start.first && end.second > start.second) {
		for (int i = 1; i < dist; i++) {
			//create piece at that location
			piece = board(std::pair<char, char>(start.first-i, start.second+i));
			if (piece != nullptr) {
				return true;
			}
		}
		return false;
	}
	else {
		return true;
	}
}

void Chess::add_to_board(std::pair<char,char> position, char c) {
	board.add_piece(position, c);
}


void Chess::remove_from_board(std::pair<char,char> position) {
	board.delete_piece(position);
}

void Chess::set_turn(bool white) {
	is_white_turn = white;
}

const Piece* Chess::get_piece(std::pair<char,char> position) {
	return board(position);
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}

std::istream& operator>> (std::istream& is, Chess& chess) {
	const Piece * piece;
	char c;
	for (char row = '8'; row >= '1'; row--) {
		for (char col = 'A'; col <= 'H'; col++) {
			is.get(c);
			if (c == '\n') {
				is.get(c);
			}
			piece = chess.get_piece(make_pair(col,row));
			if (piece && c == '-') {
				chess.remove_from_board(make_pair(col,row));
			}
			else if (piece) {
				chess.remove_from_board(make_pair(col,row));
				chess.add_to_board(make_pair(col,row), c);
			}
			else if (piece == nullptr && c != '-') {
				chess.add_to_board(make_pair(col,row), c);
			}
		}
	}
	is.get(c);
	is.get(c);
	if (c == 'w') {
		chess.set_turn(true);
	}
	if (c == 'b') {
		chess.set_turn(false);
	}

	if (chess.in_mate(chess.turn_white())) {
		cout << "Checkmate! Game over." << endl;
	}
	else if (chess.in_check(chess.turn_white())) {
		cout << "You are in check!" << endl;
	}
	else if (chess.in_stalemate(chess.turn_white())) {
		cout << "Stalemate! Game over." << endl;
	}
	return is;
}
