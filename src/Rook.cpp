// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Rook.h"
#include <cstdlib>

using std::abs;

bool Rook::legal_move_shape(std::pair<char, char> start,
                            std::pair<char, char> end) const {

  // Determine horizontal and vertical move distance
  // First element is column and second element is row
  int dist_hor = abs(end.first - start.first);
  int dist_vert = abs(end.second - start.second);

  // Either vertical or horizontal distance has to be 0
  // AND other dimension is greater than zero
  if (dist_hor == 0 && dist_vert > 0) {
    return true;
  } else if (dist_hor > 0 && dist_vert == 0) {
    return true;
  } else {
    return false;
  }
}
