// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Bishop.h"
#include <cstdlib>

using std::abs;

bool Bishop::legal_move_shape(std::pair<char, char> start,
                              std::pair<char, char> end) const {
  // Horizontal and Vertical movements have to have the same absolute value.
  int dist_hor = abs(end.first - start.first);
  int dist_vert = abs(end.second - start.second);
  return (dist_hor == dist_vert);
}
