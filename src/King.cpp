// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "King.h"
#include <cstdlib>

using std::abs;

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {

  int dist_hor = abs(end.first - start.first);
  int dist_vert = abs(end.second - start.second);

  //In both horizontal and vertical directions, king cannot move more than 1 box
  return ((dist_hor == 1 && dist_vert == 0) ||
          (dist_hor == 0 && dist_vert == 1) ||
          (dist_hor == 1 && dist_vert == 1));
}
