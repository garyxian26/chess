// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Queen.h"
#include <cstdlib>

using std::abs;

bool Queen::legal_move_shape(std::pair<char, char> start,
                             std::pair<char, char> end) const {

  // Determine horizontal and vertical move distance
  // First element is column and second element is row
  int dist_hor = abs(end.first - start.first);
  int dist_vert = abs(end.second - start.second);

  // Is the queen moving like a rook?
  if (dist_hor == 0 && dist_vert > 0) {
    return true;
  } else if (dist_hor > 0 && dist_vert == 0) {
    return true;
  }

  // Is the queen moving like a bishop?
  if (dist_hor == dist_vert) {
    return true;
  }

  // If the queen is not moving like a rook or a bishop, it's an invalid move
  return false;

}
