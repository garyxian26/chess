// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Knight.h"
#include <cstdlib>

using std::abs;

bool Knight::legal_move_shape(std::pair<char, char> start,
                              std::pair<char, char> end) const {

  // Determine horizontal and vertical move distance
  // First element is column and second element is row
  int dist_hor = abs(end.first - start.first);
  int dist_vert = abs(end.second - start.second);

  // If vertical movement is 2; then horizontal movement has to be 1;
  if (dist_vert == 2) {
    return dist_hor == 1;
  }

  // If vertical movement is 1; then horizontal movement has to be 2;
  else if (dist_vert == 1) {
    return abs(dist_hor) == 2;
  }

  // Otherwise, false
  else {
    return false;
  }
}
