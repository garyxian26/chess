// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Pawn.h"
#include <cstdlib>

using std::abs;

bool Pawn::legal_move_shape(std::pair<char, char> start,
                            std::pair<char, char> end) const {

  // Determine if it is the pawn's first move
  bool first_move =
      ((start.second == '2' && is_white()) || (start.second == '7' && !is_white()));

  // How far does the pawn want to move vertically?
  int dist_vert = end.second - start.second;

  // Check all movement possibilites
  if (abs(dist_vert) == 1) {
    // Always legal for the pawn to move forward 1 square
    return true;
  } else if (abs(dist_vert) == 2 && first_move) {
    // Pawn can ony move forward 2 squares on its first move
    return true;
  } else {
    // All other moves are illegal
    return false;
  }
}

bool Pawn::legal_capture_shape(std::pair<char, char> start,
                               std::pair<char, char> end) const {

  // Determine horizontal and vertical move distance
  // First element is column and second element is row
  int dist_hor = end.first - start.first;
  int dist_vert= end.second - start.second;

  // Determine if pawn is trying to capture
  if (abs(dist_hor) == 1 && abs(dist_vert) == 1) {
    // Pawn is moving diagonally by 1 square
    return true;
  } else if (abs(dist_hor) == 0) {
    // Pawn is trying to advance, not capture so use legal_move_shape
    return legal_move_shape(start, end);
  } else {
    // Pawn is doing something fishy; move is illegal
    return false;
  }
}
