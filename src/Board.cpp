// Gary Xian
// fxian1
//
// Jerry Wen
// fwen2
//
// Vivek Gopalakrishnan
// vgopala4

#include "Board.h"
#include "CreatePiece.h"
#include <exception>
#include <map>
#include <stdexcept>
#include <string>
#include <utility>

#include <iostream>
using std::cout;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board() {}

Board::~Board() {
  for (std::map<std::pair<char, char>, Piece *>::const_iterator it = occ.cbegin(); it != occ.cend(); ++it) {
    delete it->second;
  }
}

const Piece *Board::operator()(std::pair<char, char> position) const {
  /* Return a constant pointer to the piece at `position` if it exists, OR
   * nullptr if there is nothing there. */

  // Ensure the specified location is valid
  if (!(position.first >= 'A' && position.first <= 'H') ||
      !(position.second >= '1' && position.second <= '8')) {
    // The specified location is not on the board
    throw std::invalid_argument("Position is not on board");
  }

  // Get pointer and return
  const Piece *piece;
  try {
    piece = occ.at(position);
  }
  catch (const std::out_of_range& ex) {
    piece = nullptr;
  }
  return piece;
}

bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  /* Function to add piece to the board. Check that the piece is being sent to a
   * valid location and that the piece designation is valid. */

  if (!(position.first >= 'A' && position.first <= 'H') ||
      !(position.second >= '1' && position.second <= '8')) {
    // The specified location is not on the board
    return false;
  }

  if ((*this)(position) != nullptr) {
    // The specified location is occupied
    return false;
  }

  std::string valid_designators = "KkQqBbNnRrPpMm";
  if (valid_designators.find(piece_designator) == std::string::npos) {
    // Designated piece is invalid
    return false;
  }

  // If arguements have passed all checks, create the designated piece and the
  // specified location
  occ[position] = create_piece(piece_designator);
  return true;
}

void Board::delete_piece(std::pair<char, char> position) {

  occ.erase(position);
  delete (*this)(position);

}

bool Board::has_valid_kings() const {
  /* Check that board has 1 white king and 1 black king. */

  int num_white_kings = 0;
  int num_black_kings = 0;

  // Iterate over board. We're looking for the kings.
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      std::pair<char, char> position(c, r);
      const Piece *piece = (*this)(position);
      if (piece) {

        // If the piece exists, get its designator
        char designator = piece->to_ascii();

        // If it's a king, iterate the counter
        switch (designator) {
        case 'K':
          num_white_kings++;
          break;
        case 'k':
          num_black_kings++;
          break;
        }
      }
    }
  }

  // Return true if there is 1 black king and 1 white king
  if (num_black_kings == 1 && num_white_kings == 1) {
    return true;
  } else {
    return false;
  }
}

void Board::display() const { std::cout << *this << std::endl; }

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream &operator<<(std::ostream &os, const Board &board) {
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      const Piece *piece = board(std::pair<char, char>(c, r));
      if (piece) {
        os << piece->to_ascii();
      } else {
        os << '-';
      }
    }
    os << std::endl;
  }
  return os;
}
